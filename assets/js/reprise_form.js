require("../css/reprise_form.css");

$(function() {
    // // méthode 1 : passe la logique métier à la vue
    // $('.ui.dropdown').dropdown();
    //
    // // récupération des éléments du DOM
    // let choixMarque = $("#choixMarque .ui.dropdown");
    // let listeModele = $("#choixModele").data("listemodele");
    // let choixModele = $("#choixModele .ui.dropdown");
    //
    // // gestion du changement de choix de marque
    // choixMarque.dropdown({
    //     clearable: true,
    //     onChange: function (value, text) {
    //         choixModele.find("div.item.visible").removeClass("visible");
    //
    //         if (value) {
    //             listeModele.forEach(function (modele) {
    //                 if (modele.marque === value) {
    //                     choixModele.find("div.item[data-value="+modele.nom+"]").addClass("visible");
    //                 }
    //             });
    //             choixModele.removeClass("disabled");
    //         } else {
    //             choixModele.dropdown("clear");
    //             choixModele.addClass("disabled");
    //         }
    //     }
    // });

    // méthode 2 : le front-end interroge le back-end
    $('.ui.dropdown').dropdown();

    // récupération des éléments du DOM
    let choixMarque = $("#choixMarque .ui.dropdown");
    let choixModele = $("#choixModele .ui.dropdown");
    let urlModeles = $("#choixModele").data("url");

    // gestion du changement de choix de marque
    choixMarque.dropdown({
        clearable: true,
        onChange: function (value, text) {
            choixModele.find("div.item.visible").removeClass("visible");

            if (value) {
                choixModele.addClass("loading");
                $.ajax({
                    url: urlModeles,
                    data: { nomMarque: value },
                    method: "GET",
                    success: function (data) {
                        choixModele.removeClass("disabled");
                        choixModele.removeClass("loading");
                        data.modeles.forEach(function (modele) {
                            if (modele.marque === value) {
                                choixModele.find("div.item[data-value="+modele.nom+"]").addClass("visible");
                            }
                        });
                    },
                    error: function (err) {
                        console.error(err);
                    }
                });
            } else {
                choixModele.dropdown("clear");
                choixModele.addClass("disabled");
            }
        }
    });
});