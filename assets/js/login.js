require('../css/login.css');
require('../../node_modules/semantic-ui-css/semantic.min.css');

const $ = require('jquery');

document.addEventListener("DOMContentLoaded", function() {
    let loadableBtn = $(".loadable");

    loadableBtn.each((index, item) => {
        item.addEventListener("click", function (event) {
            $(item).addClass("disabled");
            $(item).removeClass("green");
        });
    });
});