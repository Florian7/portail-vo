<?php

declare(strict_types = 1);

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\Reprise;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class RepriseType extends AbstractType
{
    //fonction qui permet de construire le formulaire
    public function buildForm(FormBuilderInterface $builder, array $option)
    {
        //on stock la valeurs de la clé marques d'options dans la variable marques
        $marques=$option['marques'];
        $modeles=$option['modeles'];

        //données du formulaire
        $builder
            ->add('numImmat',   TextType::class, [
                "label" => "Numéro d'immatriculation* : "
            ])
            ->add('marque', ChoiceType::class, [

                'required' => false,
                'empty_data' => null,
                //on affiche le choix entre les différentes marques : affichage d'un tableau clé => valeur avec le nom de la marque en clé et en valeur
                'choices' => $marques,
            ])
            ->add('modele', ChoiceType::class, [
                'required' => false,
                'empty_data' => null,
                'choices' => $modeles,
            ])
            ->add('dateCirculation', DateType::class, [
                "label" => "Date remise en circulation : ",
                "widget" => "single_text",
                'format' => 'yyyy-MM-dd',
            ])
        ;   
    }

    public function configureOptions(optionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Reprise::class,
            'marques' => [],
            'modeles' => [],
        ]);
    }
}