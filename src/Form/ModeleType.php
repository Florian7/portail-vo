<?php

declare(strict_types = 1);

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\Modele;



class ModeleType extends AbstractType
{
    //fonction qui permet de construire le formulaire
    public function buildForm(FormBuilderInterface $builder, array $option)
    {
        //données du formulaire
        $builder
            ->add('nom',   TextType::class, [
                "label" => "Nom du modèle : "
                ])
            ->add('marque', TextType::class, [
                "label" => "Marque associée au modèle :"
            ])
        ;
    }
    public function configureOptions(optionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Modele::class,
        ]);
    }
}