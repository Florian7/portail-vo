<?php

declare(strict_types = 1);

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\Estimation;



class EstimationType extends AbstractType
{
    //fonction qui permet de construire le formulaire 
    public function buildForm(FormBuilderInterface $builder, array $option)
    {
        //données du formulaire
        $builder
            ->add('prix',     NumberType::class, [
                "label" => "estimation du véhicule : "
            ])
        ;
    }

    public function configureOptions(optionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Estimation::class,
        ]);
    }
}