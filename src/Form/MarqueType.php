<?php

declare(strict_types = 1);

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\Marque;



class MarqueType extends AbstractType
{
    //fonction qui permet de construire le formulaire
    public function buildForm(FormBuilderInterface $builder, array $option)
    {
        //données du formulaire
        $builder
            ->add('nom',   TextType::class, [
                "label" => "Nom de la marque : "
                ])
        ;
    }
    public function configureOptions(optionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Marque::class,
        ]);
    }
}