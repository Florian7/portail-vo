<?php

declare(strict_types = 1);

namespace App\Enum;

/**
 * Class LdapEnum
 */
final class LdapEnum
{
    const SAMACCOUNTNAME = "sAMAccountName";
    const FIRSTNAME = "givenName";
    const LASTNAME = "sn";
    const MAIL = "mail";
    const DEPARTMENT = "department";
    const MEMBER_OF = "memberOf";
}
