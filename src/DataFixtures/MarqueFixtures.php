<?php

declare(strict_types = 1);

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Marque;

/**
 * class MarqueFixtures
 * @package App\DataFixtures
 */
class MarqueFixtures extends Fixture
{
    // données insérées dans la bdd
    const DATA = [
        ["nom" => "Renault"],
        ["nom" => "Fiat"],
        ["nom" => "Peugeot"],
        ["nom" => "Mercedes"],
        ["nom" => "Volvo"],
        ["nom" => "Iveco"],
    ];
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void 
    {
        //parcours les différentes marques
        foreach (self::DATA as $marqueData) {
            //création d'une instance de marque
            $marque = new Marque();
            //définition des attributs
            $marque
                ->setNom($marqueData["nom"])
            ;

            //création d'une clé unique (marque-nom) pour faire une référence
            $cleUnique = "marque-{$marque->getNom()}";
            //référece de la marque
            $this->addReference($cleUnique, $marque);

            // sauvegarde de la marque en bdd
            $manager->persist($marque);
            $manager->flush();
        }
    }
}