<?php

declare(strict_types = 1);

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Modele;

/**
 * class MarqueFixtures
 * @package App\DataFixtures
 */
class ModeleFixtures extends Fixture
{
    // données insérées dans la bdd
    const DATA = [
        ["nom" => "Master",         "marque" => "Renault"],
        ["nom" => "Daily",          "marque" => "Iveco"],
        ["nom" => "Stralis",        "marque" => "Iveco"],
        ["nom" => "Eurocargo",      "marque" => "Iveco"],
        ["nom" => "Sprinter",       "marque" => "Mercedes"],
        ["nom" => "Boxer",          "marque" => "Peugeot"],
        ["nom" => "Expert",         "marque" => "Peugeot"],
    ];
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        foreach (self::DATA as $modeleData){
            $modele = new Modele();

            $modele
                ->setNom($modeleData["nom"])
                ->setMarque($modeleData["marque"])
            ;

            $cleUnique = "modele-{$modele->getNom()}";
            $this->addReference($cleUnique, $modele);

            $manager->persist($modele);
            $manager->flush();
        }
    }
}