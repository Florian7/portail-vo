<?php

declare(strict_types = 1);

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Reprise;

/**
 * class RepriseFixtures
 * @package App\DataFixtures
 */
class RepriseFixtures extends Fixture
{
    // données insérées dans la bdd
    const DATA = [
        ["numImmat" => "EF-421-CD",         "marque" => "Renault",       "modele" => "Master",      "dateCirculation" => '2019-07-21',        "completion" => 100],
        ["numImmat" => "HG-754-TZ",         "marque" => "Fiat",          "modele" => null,          "dateCirculation" => '2018-11-07',        "completion" => 75],
        ["numImmat" => "TR-642-JD",         "marque" => "Iveco",         "modele" => null,          "dateCirculation" => null,                "completion" => 50],
        ["numImmat" => "RH-651-DH",         "marque" => "Peugeot",       "modele" => "Expert",      "dateCirculation" => '2019-08-15',        "completion" => 100],
        ["numImmat" => "DZ-713-GT",         "marque" => null,            "modele" => null,          "dateCirculation" => '2017-12-03',        "completion" => 50],
        ["numImmat" => "UD-271-BE",         "marque" => "Mercedes",      "modele" => "Sprinter",    "dateCirculation" => null,                "completion" => 75],
        ["numImmat" => "EF-952-RS",         "marque" => null,            "modele" => null,          "dateCirculation" => '2018-10-05',        "completion" => 50],
        ["numImmat" => "BS-415-KD",         "marque" => "Iveco",         "modele" => "Daily",       "dateCirculation" => '2019-05-11',        "completion" => 100],
        ["numImmat" => "VQ-540-GV",         "marque" => null,            "modele" => null,          "dateCirculation" => null,                "completion" => 25],
        ["numImmat" => "XR-385-IP",         "marque" => "Iveco",         "modele" => "Stralis",     "dateCirculation" => null,                "completion" => 75],
    ];
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        // parcours des différentes reprises
        foreach (self::DATA as $repriseData) {
            // création d'une instance de reprise
            $reprise = new Reprise();
            // définition des attributs
            $reprise
                ->setNumImmat($repriseData["numImmat"])
                ->setEstiActuelle($repriseData["estiActuelle"])
                ->setMarque($repriseData["marque"])
                ->setModele($repriseData["modele"])
                ->setCompletion($repriseData["completion"])
                ->setDateCirculation($repriseData["dateCirculation"] === null ? null : new \DateTime($repriseData["dateCirculation"]))
            ;

            //création d'une clé unique (reprise-numImmat) pour faire une référence
            $cleUnique = "reprise-{$reprise->getNumImmat()}";
            // référence de la reprise
            $this->addReference($cleUnique, $reprise);

            // sauvegarde de la reprise en bdd
            $manager->persist($reprise);
            $manager->flush();
        }
    }
}