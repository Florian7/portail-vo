<?php

declare(strict_types = 1);

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Estimation;
use App\Entity\Reprise;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use App\DataFixtures\RepriseFixtures;

/**
 * class EstimationFixtures
 * @package App\DataFixtures
 */
class EstimationFixtures extends Fixture implements DependentFixtureInterface
{


    // données dans la bdd
    const DATA = [
        ["reprise" => "HG-754-TZ",      "repCompletion" => 100,        "date" => '2020-02-07',         "prix" => 2500.000,      "employe" => "edouillard"],
        ["reprise" => "TR-642-JD",      "repCompletion" => 50,         "date" => '2020-02-07',         "prix" => 2250.230,      "employe" => "bjhon"],
        ["reprise" => "RH-651-DH",      "repCompletion" => 100,        "date" => '2020-02-07',         "prix" => 2750.421,      "employe" => "tbiron"],
        ["reprise" => "DZ-713-GT",      "repCompletion" => 50,         "date" => '2020-02-07',         "prix" => 2175.512,      "employe" => "mrobert"],
        ["reprise" => "UD-271-BE",      "repCompletion" => 75,         "date" => '2020-02-07',         "prix" => 3000.565,      "employe" => "fboutin"],
        ["reprise" => "EF-952-RS",      "repCompletion" => 50,         "date" => '2020-02-07',         "prix" => 2190.145,      "employe" => "edouillard"],
        ["reprise" => "VQ-540-GV",      "repCompletion" => 25,         "date" => '2020-02-07',         "prix" => 2720.564,      "employe" => "mrobert"],
        ["reprise" => "VQ-540-GV",      "repCompletion" => 75,         "date" => '2020-02-08',         "prix" => 2950.127,      "employe" => "fboutin"],
        ["reprise" => "VQ-540-GV",      "repCompletion" => 100,        "date" => '2020-02-10',         "prix" => 2810.000,      "employe" => "edouillard"],
    ];
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {        
        foreach (self::DATA as $estimationData) {
            $estimation = new Estimation();
            //$reprise = $repriseRepo->getRepriseByImmat($repriseImmat);
            $estimation
                ->setRepCompletion($estimationData["repCompletion"])
                ->setDate(new \DateTime($estimationData["date"]))
                ->setPrix($estimationData["prix"])
                ->setEmploye($estimationData["employe"])
                ;

                //recherche de la référence de la reprise si != null
                if($refReprise = $estimationData["reprise"]){
                    /** @var Reprise $reprise */
                    $reprise = $this->getReference("reprise-".$refReprise);
                    
                    // ajout de l'estimation à la reprise
                    $reprise->addEstimation($estimation);
                }

                $reprise->setEstiActuelle($estimation);
                $manager->persist($reprise);

                $manager->persist($estimation);
                $manager->flush();
        }
    }
    public function getDependencies()
    {
        return array(
            RepriseFixtures::class,
        );
    }
}