<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\EstimationRepository")
 */
class Estimation implements \JsonSerializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $repCompletion;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=3, nullable=true)
     * @Assert\NotBlank(message="Veuillez insérer le prix de l'estimation")
     */
    private $prix;

    /**
     * @ORM\Column(type="string", length=75)
     */
    private $employe;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRepCompletion(): ?int
    {
        return $this->repCompletion;
    }

    public function setRepCompletion(int $repCompletion): self
    {
        $this->repCompletion = $repCompletion;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getPrix(): ?float
    {
        return $this->prix;
    }

    public function setPrix(?float $prix): self
    {
        $this->prix = $prix;

        return $this;
    }
    //fonction qui permet d'afficher le json et ses données
    public function jsonSerialize(): array
    {
        return [
            "prix de l'estimation" => $this->prix,
            "completion de l'estimation" => $this->repCompletion,
            "estimé par" => $this->employe,
            "date de l'estimation" => $this->date,
        ];
    }

    public function getEmploye(): ?string
    {
        return $this->employe;
    }

    public function setEmploye(string $employe): self
    {
        $this->employe = $employe;

        return $this;
    }
}
