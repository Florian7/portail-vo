<?php

declare(strict_types = 1);

namespace App\Entity;

use App\Interfaces\EntityInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="cr_ldap_user")
 * @ORM\Entity(repositoryClass="App\Repository\LdapUserRepository")
 */
class LdapUser implements EntityInterface
{
    /**
     * @var string
     *
     * @ORM\Column(name="id_employe", type="string")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $sAMAccountName;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=100, nullable=true)
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=100, nullable=true)
     */
    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @return string
     */
    public function __toString(): string
    {
        return "{$this->firstname} {$this->lastname}";
    }

    /**
     * @return string|null
     */
    public function getSAMAccountName(): ?string
    {
        return $this->sAMAccountName;
    }

    /**
     * @param string $sAMAccountName
     *
     * @return LdapUser
     */
    public function setSAMAccountName(string $sAMAccountName): self
    {
        $this->sAMAccountName = $sAMAccountName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    /**
     * @param string|null $firstname
     *
     * @return LdapUser
     */
    public function setFirstname(?string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    /**
     * @param string|null $lastname
     *
     * @return LdapUser
     */
    public function setLastname(?string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     *
     * @return LdapUser
     */
    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }
}
