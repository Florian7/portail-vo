<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RepriseRepository")
 */
class Reprise implements \JsonSerializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Veuillez insérer une immatriculation valide")
     */
    private $numImmat;

    /**
     * @ORM\ManyToMany(targetEntity="Estimation")
     * @ORM\JoinTable(name="reprises_estimations",
     *  joinColumns={@ORM\JoinColumn(name="reprise_id", referencedColumnName="id")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="estimation_id", referencedColumnName="id", unique=true)})
     */
    private $estimations;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $marque;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateCirculation;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $completion;

    /**
     * @ORM\OneToOne(targetEntity="Estimation")
     * @ORM\JoinColumn(name="estimation_id", referencedColumnName="id")
     */
    private $estiActuelle;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $modele;

    public function __construct()
    {
        $this->estimations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumImmat(): ?string
    {
        return $this->numImmat;
    }

    public function setNumImmat(string $numImmat): self
    {
        $this->numImmat = $numImmat;

        return $this;
    }
    

    //fonction qui permet d'afficher le json et ses données
    public function jsonSerialize(): array
    {
        return [
            "numImmat" => $this->numImmat,
            "estimation" => $this->estiActuelle,
            "marque" => $this->marque,
            "completion" => $this->completion,
            "dateCirculation" => $this->dateCirculation
        ];
    }

    public function getMarque(): ?string
    {
        return $this->marque;
    }

    public function setMarque(?string $marque): self
    {
        
        $this->marque = $marque;

        return $this;
    }

    public function getDateCirculation(): ?\DateTime
    {
        return $this->dateCirculation;
    }

    public function setDateCirculation(?\DateTime $dateCirculation): self
    {
        $this->dateCirculation = $dateCirculation;

        return $this;
    }

    public function getCompletion(): ?int
    {
        return $this->completion;
    }

    public function setCompletion(?int $completion): self
    {
        $this->completion = $completion;

        return $this;
    }

    public function getEstiActuelle(): ?Estimation
    {
        return $this->estiActuelle;
    }

    public function setEstiActuelle(?Estimation $estiActuelle): self
    {
        $this->estiActuelle = $estiActuelle;

        return $this;
    }

    /**
     * @return Collection|Estimation[]
     */
    public function getEstimations(): ?Collection
    {
        return $this->estimations;
    }

    public function addEstimation(Estimation $estimation): self
    {
        if (!$this->estimations->contains($estimation)) {
            $this->estimations[] = $estimation;
        }

        return $this;
    }

    public function removeEstimation(Estimation $estimation): self
    {
        if ($this->estimations->contains($estimation)) {
            $this->estimations->removeElement($estimation);
        }

        return $this;
    }

    public function getModele(): ?string
    {
        return $this->modele;
    }

    public function setModele(?string $modele): self
    {
        $this->modele = $modele;

        return $this;
    }
}
