<?php

declare(strict_types = 1);

namespace App\Service\Persistence;

use App\Interfaces\EntityInterface;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class MasterPersistence
 */
class MasterPersistence
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * MasterPersistence constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @return void
     */
    public function flush(): void
    {
        $this->em->flush();
    }

    /**
     * @param EntityInterface $entity
     * @param bool            $flush
     *
     * @return EntityInterface
     */
    public function persist(EntityInterface $entity, bool $flush = true): EntityInterface
    {
        $this->em->persist($entity);
        if ($flush) {
            $this->em->flush();
        }

        return $entity;
    }

    /**
     * @param Collection $collection
     * @param bool       $flush
     *
     * @return Collection
     */
    public function persistCollection(Collection $collection, bool $flush = true): Collection
    {
        /** @var EntityInterface $entity */
        foreach ($collection as $entity) {
            $this->em->persist($entity);
        }

        if ($flush) {
            $this->em->flush();
        }

        return $collection;
    }

    /**
     * @param EntityInterface $entity
     * @param bool            $flush
     *
     * @return EntityInterface
     */
    public function remove(EntityInterface $entity, bool $flush = true): EntityInterface
    {
        $this->em->remove($entity);
        if ($flush) {
            $this->em->flush();
        }

        return $entity;
    }

    /**
     * @param Collection $collection
     * @param bool       $flush
     */
    public function removeCollection(Collection $collection, bool $flush = true): void
    {
        /** @var EntityInterface $entity */
        foreach ($collection as $entity) {
            $this->em->remove($entity);
        }

        if ($flush) {
            $this->em->flush();
        }
    }

    /**
     * @param EntityInterface $entity
     */
    public function refresh(EntityInterface $entity): void
    {
        $this->em->refresh($entity);
    }
}
