<?php

declare(strict_types = 1);

namespace App\Service\Metier;

use App\Entity\Reprise;

/**
 * Class CompletionMetier
 */
class CompletionMetier
{
    /**
     * @param Reprise $reprise
     */
    //fonction qui permet de modifier et d'enregistrer la completion de la reprise enregistrée
    public function tauxCompletion(Reprise $reprise): int
    {
        //on initialise le nombre d'informations connues à 0
        $information = 0;

        $numImmat = $reprise->getNumImmat();
            
        if ($numImmat !== null)
        {
            $information = $information +1;
        }

        $marque = $reprise->getMarque();
        if ($marque !== null)
        {
            $information = $information +1;
        }

        $dateCirculation = $reprise->getDateCirculation();
        if ($dateCirculation !== null)
        {
            $information = $information +1;
        }

        $modele = $reprise->getModele();
        if ($modele !== null)
        {
            $information = $information +1;
        }
            
            //une fois les informations récupérées ou non, on calcul le taux de complétion pour pouvoir l'enregistrer pr la suite
            $completion = $information/4*100;
            $completion = intval(round($completion));
            
            //au cas ou on est plus obligé de rentré au minimum l'immatricalution 
            if ($completion == null)
            {
                $completion = 0;
            }
            
            return $completion;
    }
}