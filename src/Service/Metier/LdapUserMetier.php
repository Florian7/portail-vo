<?php

declare(strict_types = 1);

namespace App\Service\Metier;

use App\Entity\LdapUser;
use App\Repository\LdapUserRepository;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class LdapUserMetier
 */
class LdapUserMetier
{
    /**
     * @var Security
     */
    private $security;

    /**
     * @var LdapUserRepository
     */
    private $ldapUserRepo;

    /**
     * @var LdapUser
     */
    private $user;

    /**
     * LdapUserMetier constructor.
     * @param Security           $security
     * @param LdapUserRepository $ldapUserRepository
     */
    public function __construct(Security $security, LdapUserRepository $ldapUserRepository)
    {
        $this->security = $security;
        $this->ldapUserRepo = $ldapUserRepository;
    }


    /**
     * @return LdapUser|null
     */
    public function getUser(): ?LdapUser
    {
        if (!$this->user instanceof LdapUser) {
            // get original user
            /** @var UserInterface */
            $originalUser = $this->security->getUser();
            if ($originalUser instanceof UserInterface) {
                // fetch the corresponding LdapUser
                $this->user = $this->ldapUserRepo->findBysAMAccountName($originalUser->getUsername());
            }
        }

        return $this->user;

    }
}