<?php

declare(strict_types = 1);

namespace App\Service\Ldap;

use App\Entity\LdapUser;
use App\Enum\LdapEnum;
use Symfony\Component\Ldap\Entry;
use Symfony\Component\Ldap\Ldap;

/**
 * Class LdapReader
 */
class LdapReader
{
    /**
     * @var Ldap
     */
    private $masterLdap;

    /**
     * @var string
     */
    private $baseDn;

    /**
     * @var string
     */
    private $userDn;

    /**
     * @var string
     */
    private $userPassword;

    /**
     * LdapReader constructor.
     * @param Ldap   $masterLdap
     * @param string $baseDn
     * @param string $userDn
     * @param string $userPassword
     */
    public function __construct(Ldap $masterLdap, string $baseDn, string $userDn, string $userPassword)
    {
        $this->masterLdap = $masterLdap;
        $this->baseDn = $baseDn;
        $this->userDn = $userDn;
        $this->userPassword = $userPassword;
    }

    /**
     * @param LdapUser $ldapUser
     *
     * @return LdapUser
     *
     * @throws \Exception
     */
    public function update(LdapUser $ldapUser): LdapUser
    {
        // fetch raw user info from LDAP server
        $entry = $this->fetchUserFromsAMAccountName($ldapUser->getSAMAccountName());

        // get fistname, lastname and mail from ldap
        $firstname  = $entry->getAttribute(LdapEnum::FIRSTNAME)[0];
        $lastname   = $entry->getAttribute(LdapEnum::LASTNAME)[0];
        $mail       = $entry->getAttribute(LdapEnum::MAIL)[0];

        // update user attributes
        $ldapUser
            ->setFirstname($firstname)
            ->setLastname($lastname)
            ->setEmail($mail)
        ;

        return $ldapUser;
    }

    /**
     * @param string $sAMAccountName
     *
     * @return Entry
     */
    public function fetchUserFromsAMAccountName(string $sAMAccountName): Entry
    {
        // remove all special characters from the string
        $sAMAccountName = preg_replace('/[^A-Za-z0-9\-]/', '', $sAMAccountName);

        // check if the string is not empty
        if (empty($sAMAccountName)) {
            throw new \Exception("l'identifiant de connexion LDAP \"{$sAMAccountName}\" n'est pas conforme");
        }

        // bind connection to LDAP server
        $this->masterLdap->bind($this->userDn, $this->userPassword);

        // create the query string and the query
        $queryString = "(&(sAMAccountName={$sAMAccountName}))";
        $query = $this->masterLdap->query($this->baseDn, $queryString);

        // get the result from the query
        $result = $query->execute()->toArray();

        // if the first result ins't an LDAP Entry
        if (count($result) < 1) {
            throw new \Exception("aucun résultat n'a été trouvé pour l'identifiant de connexion LDAP \"{$sAMAccountName}\"");
        }

        return $result[0];
    }
}
