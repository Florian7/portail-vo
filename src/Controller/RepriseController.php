<?php

declare(strict_types = 1);

namespace App\Controller;

use App\Entity\Estimation;
use App\Entity\Reprise;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Form\RepriseType;
use App\Repository\RepriseRepository;
use App\Form\EstimationType;
use App\Repository\MarqueRepository;
use App\Repository\ModeleRepository;
use App\Service\Metier\CompletionMetier;
use App\Service\Metier\LdapUserMetier;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;


/**
 * Class RepriseController
 */
class RepriseController extends AbstractMasterController
{
    /**
     * @Route ("/")
     *
     * @return Response
     */
    //page d'accueil de portail VO
    public function index(): Response
    {
        return $this->render("index.html.twig");
    }
    /**
     * @param repriseRepository $repriseRepo
     * @Route ("/reprise")
     * @return Response
     */
    //fonction qui pemret d'affucher la liste des reprises
    public function listeAction(RepriseRepository $repriseRepo): Response
    {
        $reprise = $repriseRepo->findAll();

        return $this->render("Reprise/liste.html.twig", [
            "reprises" => $reprise,
        ]);
    }
    /**
     * @param int $repriseId
     * @Route(
     *      "/reprise/detail-{repriseId}",
     *      requirements={"repriseId":"\d+"}
     * )
     * @return Response
     */
    //fontion qui permet d'afficher le détail d'une reprise
    public function detailAction(int $repriseId, RepriseRepository $repriseRepo): Response
    {
        $reprise = $repriseRepo->getRepriseById($repriseId);
        
        return $this->render("Reprise/detail.html.twig", array(
            'reprise' => $reprise
        ));

    }
    /**
     * @Route(
     *      "/reprise/add/esti-{repriseId}", 
     *      requirements={"repriseId":"\d+"}
     * )
     * @return Request
     */
    //fonction qui permet d'ajouter une nouvelle estimation 
    public function estimationAction(int $repriseId, RepriseRepository $repriseRepo, Request $request, EntityManagerInterface $em): Response
    {
        $estimation = new Estimation;
        $reprise = $repriseRepo->getRepriseById($repriseId);
        $form = $this->createForm(EstimationType::class, $estimation);

        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            //récupération de la completion de la reprise
            $completion = $reprise->getCompletion();

            if ($reprise->getEstiActuelle() != null ){

                //récupération du prix de l'estimation en train de se faire
                $newPrix = $estimation->getPrix();

                //récupération du prix et la completion de la dernière estimation
                $completionEsti = $reprise->getEstiActuelle()->getRepCompletion();
                $prix = $reprise->getEstiActuelle()->getPrix();

                if ($newPrix == $prix && $completion == $completionEsti){
                    throw new \Exception("Une erreur est survenue durant l'ajout d'une nouvelle estimation, le prix ".$newPrix."€ est le même que le précédent");
                }
            }

            //ajout de la nouvelle estimation à la collection
            $reprise->addEstimation($estimation);

            //on écrase la dernière estimation par la nouvelle
            $reprise->setEstiActuelle($estimation);

            //ajout de la complétion à l'estimation
            $estimation->setRepCompletion($completion);

            //ajout de la date actuelle
            $estimation->setDate(new \DateTime());

            //ajout de l'employe
            $LdapUser = $this->getLdapUser();
            $employe = $LdapUser->getSAMAccountName();
            $estimation->setEmploye($employe);

            //sauvegarde dans la bdd
            $em->persist($estimation);

            $em->persist($reprise);
            $em->flush();

            $this->addFlash('notice', 'Les infos ont bien été modifié.');

            return $this->redirectToRoute('app_reprise_liste');
        }
        return $this->render('Reprise/estimation.html.twig', array(
            'reprise' => $reprise,
            'form' => $form->createView(),
        ));
    }
    /**
     * @Route(
     *      "/reprise/edit/infos-{repriseId}", 
     *      requirements={"repriseId":"\d+"}
     * )
     * @return Request
     */
    //fonction qui permet de modifier une reprise
    public function editAction(int $repriseId, RepriseRepository $repriseRepo, Request $request, EntityManagerInterface $em, MarqueRepository $marque, CompletionMetier $completionMetier, ModeleRepository $modeleRepo): Response
    {
        $reprise = $repriseRepo->getRepriseById($repriseId);

        return $this->gestionForm($em, false, $reprise, $request, $marque, $completionMetier, $modeleRepo);
        

    }
    /**
     * @Route("/add")
     * @return Request
     */
    //fonction qui permet d'ajouter une nouvelle reprise
    public function addAction(EntityManagerInterface $em, Request $request, MarqueRepository $marqueRepo, CompletionMetier $completionMetier, ModeleRepository $modeleRepo): Response
    {
        $reprise = new Reprise();
        

        return $this->gestionForm($em, true, $reprise, $request, $marqueRepo, $completionMetier, $modeleRepo);
        
        
        
    }
    //function privée qui permet d'accéder au formulaire de reprise et d'ajouter/modifier les données
    private function gestionForm(EntityManager $em, bool $ajouter, Reprise $reprise, Request $request, MarqueRepository $marqueRepo, CompletionMetier $completionMetier, ModeleRepository $modeleRepo): Response
    {
        //on récupère la liste des marques
        $listeMarque = $marqueRepo->findAll();
        //on créer un nouveau tableau ou l'on stockera le nom des marques en tant que clé et valeur
        $marques = [];
        //on parcours la liste des marques
        foreach($listeMarque as $marque)
        {
            //on va cherche le nom de la marque que l'on stock
            $nom = $marque->getNom();
            //on ajoute le nom en tant que clé et valeur
            $marques[$nom] = $nom;
        }
        
        //on récupère la liste des modèles
        $listeModele = $modeleRepo->findAll();
        $modeles = [];
        foreach($listeModele as $modele)
        {
            $nomModele = $modele->getNom();
            $modeles[$nomModele] = $nomModele;
        }

        $form = $this->createForm(RepriseType::class, $reprise, [
            'marques' => $marques,
            'modeles' => $modeles
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){

            //appel le service de completion qui permet d'ajouter/modifier le % de completion en temps réel
            $completion = $completionMetier->tauxCompletion($reprise);
            $reprise->setCompletion($completion);

            $em->persist($reprise);
            $em->flush();


            if ($ajouter == true){      
                $this->addFlash('notice', 'La reprise a bien été ajouté.');
                $this->addFlash('notice', 'Les informations ont été remplis à '.$completion.' %.');

                return $this->redirectToRoute('app_reprise_index');
            }else{
                $this->addFlash('notice', 'Les infos ont bien été modifié.');
                $this->addFlash('notice', 'Les informations ont été remplis à '.$completion.' %.');
                return $this->redirectToRoute('app_reprise_liste');
            }
        }

        return $this->render("Reprise/form.html.twig", [
            'reprise' => $reprise,
            'ajouter' => $ajouter,
            'form' => $form->createView(),
            'listeModele' => $listeModele,
        ]);
    }
    /**
     * @Route("/api/{repriseImmat}")
     * @param Request $request
     * return Response
     */
    //fonction qui permet de retourner les données d'une reprise en json sur une API grace à partir de l'immatriculation
    public function donneesAction( RepriseRepository $repriseRepo, $repriseImmat): Response
    {
        //récupération des infos du vehicule par son numImmat
        
        $reprise = $repriseRepo->getRepriseByImmat($repriseImmat);

        return new JsonResponse($reprise);
    }
}