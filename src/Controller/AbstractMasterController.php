<?php

declare(strict_types = 1);

namespace App\Controller;

use App\Entity\LdapUser;
use App\Service\Metier\LdapUserMetier;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Class AbstractMasterController
 */
class AbstractMasterController extends AbstractController
{
    /**
     * @var LdapUserMetier
     */
    private $ldapUserMetier;

    /**
     * AbstractMasterController constructor.
     * @param LdapUserMetier $ldapUserMetier
     */
    public function __construct(LdapUserMetier $ldapUserMetier)
    {
        $this->ldapUserMetier = $ldapUserMetier;
    }


    /**
     * @return LdapUser
     */
    public function getLdapUser(): LdapUser
    {
        // fetch the current connected LdapUser
        $ldapUser = $this->ldapUserMetier->getUser();

        // if no LdapUser was found
        if (!$ldapUser instanceof LdapUser) {
            throw new \Exception("une erreur est survenue durant la recherche des informations de l'utilisateur");
        }

        return $ldapUser;
    }
}