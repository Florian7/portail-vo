<?php

declare(strict_types = 1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Class LoginController
 */
class LoginController extends AbstractController
{
    /**
     * @Route("/login")
     *
     * @param AuthenticationUtils $authenticationUtils
     *
     * @return Response
     */
    public function loginAction(AuthenticationUtils $authenticationUtils): Response
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the admin
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render("login.html.twig", [
            "lastUsername" => $lastUsername,
            "error" => $error,
        ]);
    }

    /**
     * @Route("/logout")
     */
    public function logoutAction(): void
    {
        throw new \Exception("Une erreur est survenue durant la déconnexion.");
    }
}
