<?php

declare(strict_types = 1);

namespace App\Controller;

use App\Entity\Marque;
use App\Form\MarqueType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;



/**
 * class MarqueController
 */
class MarqueController extends AbstractController
{
    /**
     * @Route ("/marque/add")
     * 
     * @return Response
     */
    //fonction qui permet d'ajouter une marque inexistante
    public function addAction(EntityManagerInterface $em, Request $request): Response
    {
        $marque = new Marque();

        $form = $this->createForm(MarqueType::class, $marque);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){

            $em->persist($marque);
            $em->flush();

            $this->addFlash('notice', 'La marque a bien été ajoutée dans notre base de données');

            return $this->redirectToRoute('app_reprise_liste');
        }
        return $this->render("Marque/form.html.twig", [
            'marque' => $marque,
            'form' => $form->createView(),
        ]);
    }
}