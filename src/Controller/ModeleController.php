<?php

declare(strict_types = 1);

namespace App\Controller;

use App\Entity\Modele;
use App\Form\ModeleType;
use App\Repository\ModeleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * class MarqueController
 * @Route("/modeles")
 */
class ModeleController extends AbstractController
{
    /**
     * @Route("/add")
     * 
     * @return Response
     */
    //fonction qui permet d'ajouter une modele inexistante
    public function addAction(EntityManagerInterface $em, Request $request): Response
    {
        $modele = new Modele();

        $form = $this->createForm(ModeleType::class, $modele);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){

            $em->persist($modele);
            $em->flush();

            $this->addFlash('notice', 'Le modèle a bien été ajoutée dans notre base de données');

            return $this->redirectToRoute('app_reprise_liste');
        }
        return $this->render("Modele/form.html.twig", [
            'modele' => $modele,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/marque", methods={"GET"})
     *
     * @return Response
     */
    public function modelesPourMarque(ModeleRepository $repo, Request $request): Response
    {
        // récupération du nom de la marque
        $nomMarque = $request->get("nomMarque");
        if (empty($nomMarque)) {
            return new JsonResponse([], 500);
        }
        // récupération de tous les modèles qui contiennent la marque
        $modeles = $repo->getModelesByMarque($nomMarque);

        return new JsonResponse([
            "modeles" => $modeles,
        ], 200);
    }
}