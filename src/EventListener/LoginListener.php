<?php

declare(strict_types = 1);

namespace App\EventListener;

use App\Entity\LdapUser;
use App\Repository\LdapUserRepository;
use App\Service\Ldap\LdapReader;
use App\Service\Persistence\MasterPersistence;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

/**
 * Class LoginListener
 */
class LoginListener
{
    /**
     * @var LdapReader
     */
    private $ldapReader;

    /**
     * @var LdapUserRepository
     */
    private $ldapUserRepo;

    /**
     * @var MasterPersistence
     */
    private $persistence;

    /**
     * LoginListener constructor.
     * @param LdapReader         $ldapReader
     * @param LdapUserRepository $ldapUserRepo
     * @param MasterPersistence  $persistence
     */
    public function __construct(LdapReader $ldapReader, LdapUserRepository $ldapUserRepo, MasterPersistence $persistence)
    {
        $this->ldapReader = $ldapReader;
        $this->ldapUserRepo = $ldapUserRepo;
        $this->persistence = $persistence;
    }

    /**
     * @param InteractiveLoginEvent $event
     */
    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        // get the user that is logged in
        /** @var UserInterface $user */
        $user = $event->getAuthenticationToken()->getUser();

        // fetch the corresponding RhUser in the database
        $ldapUser = $this->ldapUserRepo->findBysAMAccountName($user->getUsername());

        // if no user was found corresponding to this account name, presume
        // it's his first connetion
        if (!$ldapUser instanceof LdapUser) {
            // create a new LdapUser
            $ldapUser = new LdapUser();
            $ldapUser->setSAMAccountName($user->getUsername());
        }

        // use the LDAP Reader to fetch current informations about the user
        // in order to update date stored in the database
        $ldapUser = $this->ldapReader->update($ldapUser);

        // persist the LdapUser to the database
        $this->persistence->persist($ldapUser);
    }
}
