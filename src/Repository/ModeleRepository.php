<?php

namespace App\Repository;

use App\Entity\Modele;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;


class ModeleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Modele::class);
    }
    /**
     * @param int modeleId
     * @return Modele
     */
    public function getModeleeById($modeleId): Modele
    {
        if (!is_numeric($modeleId) || $modeleId <= 0){
            throw new \Exception("Impossible d'obtenir le détail de la marque ({$modeleId}).");
        }
        $qb = $this->createQueryBuilder("mod");

        $qb
            ->where("mod.id = :idModele")
            ->setParameter("idModele", $modeleId)
        ;
        return $qb->getQuery()->getSingleResult();
    }

    /**
     * @param string $nomMarque
     *
     * @return array
     */
    public function getModelesByMarque(string $nomMarque): array
    {
        $qb = $this->createQueryBuilder("modele");

        $qb
            ->where($qb->expr()->eq("modele.marque", ":marque"))
            ->setParameter("marque", $nomMarque)
        ;

        return $qb->getQuery()->getResult();
    }

    /**
     * @return array
     */
    public function findAll(): array
    {
        return parent::findAll();
    }
}
