<?php

namespace App\Repository;

use App\Entity\Estimation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class Estimationrepository
 * @package App\Repository
 */
class EstimationRepository extends ServiceEntityRepository
{
    /**
     * EstimationRepository constructor
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Estimation::class);
    }
    /**
     * @return array
     */
    public function findAll(): array
    {
        return parent::findAll();
    }
}
