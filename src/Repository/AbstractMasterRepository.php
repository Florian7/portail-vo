<?php

declare(strict_types = 1);

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class AbstractMasterRepository
 */
class AbstractMasterRepository extends ServiceEntityRepository
{
    /**
     * AbstractMasterRepository constructor.
     * @param ManagerRegistry $registry
     * @param string          $entityClass
     */
    public function __construct(ManagerRegistry $registry, string $entityClass)
    {
        parent::__construct($registry, $entityClass);
    }

    /**
     * @return Collection
     */
    public function findAll(): Collection
    {
        return new ArrayCollection(parent::findAll());
    }

    /**
     * @param QueryBuilder $qb
     *
     * @return Collection
     */
    protected function getCollection(QueryBuilder $qb): Collection
    {
        return new ArrayCollection($qb->getQuery()->getResult());
    }
}
