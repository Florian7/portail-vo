<?php

declare(strict_types = 1);

namespace App\Repository;

use App\Entity\LdapUser;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class LdapUserRepository
 */
class LdapUserRepository extends AbstractMasterRepository
{
    /**
     * LdapUserRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LdapUser::class);
    }

    /**
     * @param string $sAMAccountName
     *
     * @return LdapUser|null
     */
    public function findBysAMAccountName(string $sAMAccountName): ?LdapUser
    {
        $qb = $this->createQueryBuilder("user");

        $qb
            ->where($qb->expr()->eq("user.sAMAccountName", ":sAMAccountName"))
            ->setParameter("sAMAccountName", $sAMAccountName)
        ;

        return $qb->getQuery()->getOneOrNullResult();
    }
}
