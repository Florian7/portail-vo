<?php

namespace App\Repository;

use App\Entity\Reprise;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class RepriseRepository
 * @package App\Repository
 */
class RepriseRepository extends ServiceEntityRepository
{
    /**
     * RepriseRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Reprise::class);
    }
    /**
     * @return array
     */
    public function findAll(): array
    {
        return parent::findAll();
    }

    /**
     * @param int repriseId
     * @return Reprise
     */
    //fonction qui permet de récupérer les données d'une reprise grâce à son id
    public function getRepriseById($repriseId): Reprise
    {
        if (!is_numeric($repriseId) || $repriseId <= 0){
            throw new \Exception("Impossible d'obtenir de détail de la reprise ({$repriseId}).");
        }
        $qb = $this->createQueryBuilder("rep");

        $qb
            ->addSelect(["estim"])
            ->leftJoin("rep.estimations","estim")
            ->where("rep.id = :idReprise")
            ->setParameter("idReprise", $repriseId)
        ;
        return $qb->getQuery()->getOneOrNullResult();
    }
    /**
     * @param int repriseId
     * @return Reprise
     */
    //fonction qui permet de récupérer les données d'une reprise grâce à son immatriculation
    public function getRepriseByImmat($repriseImmat): ?Reprise
    {
        $qb = $this->createQueryBuilder("rep");

        $qb
            ->where("rep.numImmat = :immatReprise")
            ->setParameter("immatReprise", $repriseImmat)
        ;
        return $qb->getQuery()->getOneOrNullResult();
    }
    /**
     * @param int repriseId
     * @return Reprise
     */
    public function getEstiByReprise($repriseId): ?Reprise
    {
        $qb = $this->createQueryBuilder("rep");

        $qb
            ->addSelect(["estim"])
            ->leftJoin("rep.estimations","estim")
            ->where("rep.id = :idRep")
            ->setParameter("idRep", $repriseId)
            ;
        return $qb->getQuery()->getOneOrNullResult();
    }
}
