<?php

namespace App\Repository;

use App\Entity\Marque;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class MarqueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Marque::class);
    }
    /**
     * @param int marqueId
     * @return Marque
     */
    //function qui permet de récupérer le sdonnées d'une marque grâce à son id
    public function getMarqueById($marqueId): Marque
    {
        if (!is_numeric($marqueId) || $marqueId <= 0){
            throw new \Exception("Impossible d'obtenir le détail de la marque ({$marqueId}).");
        }
        $qb = $this->createQueryBuilder("mar");

        $qb
            ->where("mar.id = :idMaque")
            ->setParameter("idMarque", $marqueId)
        ;
        return $qb->getQuery()->getSingleResult();
    }
    /**
     * @return array
     */
    public function findAll(): array
    {
        return parent::findAll();
    }
}
