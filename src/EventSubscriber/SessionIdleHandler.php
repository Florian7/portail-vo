<?php

declare(strict_types = 1);

namespace App\EventSubscriber;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Csrf\TokenStorage\TokenStorageInterface;

/**
 * Class SessionIdleHandler
 */
class SessionIdleHandler
{
    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var TokenStorageInterface
     */
    private $securityToken;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var int
     */
    private $maxIdleTime;

    /**
     * SessionIdleHandler constructor.
     * @param SessionInterface      $session
     * @param TokenStorageInterface $securityToken
     * @param RouterInterface       $router
     * @param int                   $maxIdleTime
     */
    public function __construct(SessionInterface $session, TokenStorageInterface $securityToken, RouterInterface $router, int $maxIdleTime)
    {
        $this->session = $session;
        $this->securityToken = $securityToken;
        $this->router = $router;
        $this->maxIdleTime = $maxIdleTime;
    }

    /**
     * @param RequestEvent $event
     */
    public function onKernelRequest(RequestEvent $event)
    {
        // if there isn't the master request
        if (HttpKernelInterface::MASTER_REQUEST !== $event->getRequestType()) {
            return;
        }

        // if a max activity time period is set
        if ($this->maxIdleTime > 0) {
            $this->session->start();

            // calculate the number of second of inactivity
            $incativityTime = time()-$this->session->getMetadataBag()->getLastUsed();

            // if this number is greater than the max idle time allowed
            if ($incativityTime > $this->maxIdleTime) {
                $this->securityToken->setToken(null, null);

                // generate the route to logout for the given firewall context
                $logoutRoute = $this->router->generate("app_login_logout");

                // send response to the corresponding logout route
                $event->setResponse(new RedirectResponse($logoutRoute));
            }
        }
    }
}
